CaveTalk with IkaNama(IkaNama対応版壁トーーク) 
======================
  
これは<https://github.com/madguy/CaveTalk/releases>のforkプロジェクトです。  
元ツールであるCavetalkに、IkaNama(自動IkaLogコメントツール)を対応させたものです。  
License等はCavetalkに準じます。  
また、原作者様から公開停止を命じられた場合、即座に公開を停止します。  
連絡は僕のTwitterにお願いします。  
<https://twitter.com/happou31>  
  
IkaNamaとは？  
----------------
スプラトゥーン画面認識型戦績記録ツール"IkaLog"からのデータを利用して、自動でコメントを投稿するツール。  
  
使い方  
----------------
1.IkaLogのWebsocketServerを有効にする  
2.IkaLogを起動する  
3.このソースからビルドしたCavetalkでCavetubeにログインする  
4.配信を開始してCavetalkで自分のページに接続する  
5.メニューのIkaNamaから開くボタンを押す  
6.「未接続」と書かれたボタンを押す  
7.イカをプレイする  
8.なんか投稿されたら成功  
  
ビルドするのめんどくさい人向けバイナリダウンロード  
-----------------
<https://onedrive.live.com/redir?resid=6AED9E65836633BB!14351&authkey=!AEW_tVHWHD3i3oU&ithint=folder%2czip>  
  
使用上の注意  
-----------------
・Cavetalk本体でログインしているかつ、Cavetalkの接続先が自身の配信でないと動かないようになっています。  
・倒しすぎたり死にすぎたりすると、Cavetube側のコメント投稿数制限に引っかかる恐れがあるので、必要であればCavetalkメニューから開けるIkaNamaのオプション画面で、投稿するシーンを「on_result_detail」のみにするなどの対策を各自行ってください。  
・運営から何か警告されたら使用するのをやめてください。(お手数ですが、そのようなことがあった場合、その旨を僕にも伝えていただけたら嬉しいです)  
  
以下、オリジナルのREADME.md
  

CaveTalk(壁トーーク) 
======================  
  
Download
----------------
<https://github.com/madguy/CaveTalk/releases>

License
-------

Copyright 2011-2015, まどがい  
MIT-style License.  
<http://www.opensource.org/licenses/mit-license.php>


ParallelExtensionsExtras  
Author: Microsoft Corporation  

Apache License, Version 2.0  
<http://www.apache.org/licenses/>


Connect Icon  
Author: David Vignoni  
<http://www.icon-king.com/>

LGPL License Version 3  
<http://www.gnu.org/copyleft/lesser.html>


WebSocket4Net  
<http://websocket4net.codeplex.com/>

Apache License, Version 2.0  
<http://www.apache.org/licenses/>


SocketIoClientDotNet
<https://github.com/Quobject/SocketIoClientDotNet>

MIT-style License.  
<http://www.opensource.org/licenses/mit-license.php>

概要
----

[CaveTube](http://gae.cavelis.net/)用のコメント読み上げソフトです。  

システム要件
------------

以下の環境が必要です。

* [.NET Framework 4.5](http://www.microsoft.com/ja-jp/download/details.aspx?id=30653)
* [棒読みちゃん](http://chi.usamimi.info/Program/Application/BouyomiChan/)

※ 棒読みちゃんの代わりに[SofTalk](http://www35.atwiki.jp/softalk/)に読ませることも可能です。

使い方
------

放送URLにCaveTubeの視聴URL、または部屋IDを入力して接続ボタンを押すことで放送に接続できます。  
新しいコメントがサーバから送られてくると棒読みちゃんにリクエストを送信します。

CaveTalkを起動する前に棒読みちゃんを起動しておくと便利です。

CaveTubeの新規配信やコメントをポップアップ表示する機能などもオプションで設定できます。

既知の不都合
------------
一部のウィルス対策ソフトが入っていると動作しません。(AVG2012など)  
これはウィルス対策ソフトが、CaveTubeが使用しているWebSocketという通信方法を塞いでしまうためです。