﻿using System.Windows;
using System.Windows.Controls;

namespace Hapo31.IkaNama.View
{
	/// <summary>
	/// IkaNama.xaml の相互作用ロジック
	/// </summary>
	public partial class IkaNamaView : Window
	{
		public IkaNamaView()
		{
			InitializeComponent();
		}

		private void logTextBox1_TextChanged(object sender, TextChangedEventArgs e)
		{
			textBoxScrollViewer.ScrollToBottom();
		}
	}
}
